import argparse

def validate_baudrate(baudrate):
    return 115200

def foo():
    print("bar")

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Add some integers.')
    parser.add_argument('--port', '-p', help='serial port name to open')
    parser.add_argument('--baudrate', '-b', help='serial port baudrate')
    args = parser.parse_args()
    port = vars(args)['port']
    baudrate = vars(args)['baudrate']
    print(port)
    print(baudrate)