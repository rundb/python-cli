# Python template for CLI tools

This project is designed to be a template for Python-based CLI tools. 

It includes:
- minimal code for unit tests' execution
- minimal multi-folder import configuration
- command-line arguments parser example.

## Unit-tests execution

Execute command `python tests/tool_tests.py`

## CLI application execution

Execute: `python tool/tool.py --port /dev/ttyUSB0 --baudrate 115200`

(Yes, mainly I design tools for interfacing UART, so demo is also prepared for UART)