import sys, os
import unittest

sys.path.append("./")
from tool.tool import validate_baudrate

class TestTool(unittest.TestCase):

    def test_baudrate_validation(self):
        self.assertEqual(validate_baudrate('115200'), 115200)
        self.assertEqual(validate_baudrate('9600'), 9600)

if __name__ == '__main__':
    unittest.main()
    